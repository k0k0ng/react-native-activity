import React from 'react';
import { 
  StyleSheet, 
  Text, 
  View,
} from 'react-native';

import {createStackNavigator} from 'react-navigation'


import LoginScreen from "./screen/LoginScreen"
import HomeScreen from "./screen/HomeScreen"
import DSScreen from "./screen/DSScreen"
import ActivityIndicator from "./screen/ActivityIndicator"
import Button from "./screen/Button"
import DrawerLayout from "./screen/DrawerLayout"
import FlatList from "./screen/FlatList"
import Image from "./screen/Image"
import KeyboardAvoidingView from "./screen/KeyboardAvoidingView"
import ListView from "./screen/ListView"
import Modal from "./screen/Modal"
import Picker from "./screen/Picker"
import ProgressBar from "./screen/ProgressBar"
import RefreshControl from "./screen/RefreshControl"
import SafeArea from "./screen/SafeArea"
import ScrollViewPage from "./screen/ScrollViewPage"
import SectionList from "./screen/SectionList"
import StatusBar from "./screen/StatusBar"
import Switch from "./screen/Switch"
import Text_TextInp from "./screen/Text_TextInp"
import TouchableHighlight from "./screen/TouchableHighlight"
import ViewPage from "./screen/ViewPage"
import PageAndroid from "./screen/PageAndroid"
import WebView from "./screen/WebView"


export default class App extends React.Component {
  render() {
    return (
      <AppNavigator/>
    );
  }
}


const AppNavigator = createStackNavigator({

  LoginScreen: { screen: LoginScreen},
  HomeScreen: { screen: HomeScreen},
  DSScreen: {screen: DSScreen},
  ActivityIndicator: {screen: ActivityIndicator},
  Button: {screen: Button},
  DrawerLayout: {screen: DrawerLayout},
  FlatList: {screen: FlatList},
  Image: {screen: Image},
  KeyboardAvoidingView: {screen: KeyboardAvoidingView},
  ListView: {screen: ListView},
  Modal: {screen: Modal},
  Picker: {screen: Picker},
  ProgressBar: {screen: ProgressBar},
  RefreshControl: {screen: RefreshControl},
  SafeArea: {screen: SafeArea},
  ScrollViewPage: {screen: ScrollViewPage},
  SectionList: {screen: SectionList},
  StatusBar: {screen: StatusBar},
  Switch: {screen: Switch},
  Text_TextInp: {screen: Text_TextInp},
  TouchableHighlight: {screen: TouchableHighlight},
  ViewPage: {screen: ViewPage},
  PageAndroid: {screen: PageAndroid},
  WebView: {screen: WebView}

})


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
