import React, { Component } from 'react';
import { Text, View, StyleSheet, AppRegistry } from "react-native";


export default class DetailScreenPage extends Component {
    render() {
        return (
            <View>
                <Text>This is the Detail Screen :)</Text>
            </View>
        );
    }
}


AppRegistry.registerComponent('DetailScreenPage', () => DetailScreenPage)