import React, { Component } from 'react';
import { FlatList, View, Text, StyleSheet } from "react-native";

class FlatListPage extends Component {
    render() {
        return (
            <View>
            <FlatList
            data={[
                {key: 'Lupang Hinirang'}, 
                {key: 'Bayang magiliw'},
                {key: 'Perlas nang silanganan'}
                
            ]}
            renderItem={({item}) => <Text style={styles.item}>{item.key}</Text>}
            />
            </View>
        );
    }
}

const styles = StyleSheet.create({

    item: {
        paddingRight: 10,
        paddingLeft: 10,
        fontSize: 16,
        height: 40
    },
})

export default FlatListPage;