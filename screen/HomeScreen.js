import React, { Component } from "react";
import{
    View,
    Text,
    StyleSheet
} from "react-native";

import {createMaterialTopTabNavigator} from 'react-navigation'

class HomeScreen extends Component{
    render(){
        return(
            <View>
            <Text> This is the home screen.</Text>
            </View>
        );
    }
}

class Notifications extends Component{
    render(){
        return(
            <View>
                <Text>This is Notification page.</Text>
            </View>
        );
    }
} 


const HomeScreenTabNavigator = createMaterialTopTabNavigator({

    HomeScreen: {
        screen: HomeScreen
    },
    Notifications: {
        screen: Notifications
    }
},{
    animationEnabled: true
})

export default HomeScreenTabNavigator;