import React, { Component } from "react";
import{
    View,
    Text,
    StyleSheet,
    Button,
    ScrollView
} from "react-native";

import ActivityIndicator from "./ActivityIndicator"


class LoginScreen extends Component{
    render(){
        return(
            <ScrollView>
            <View>
            <Text> </Text>
                <Button onPress={() => this.props.navigation.navigate('DSScreen')} title="Go to Detail Screen"></Button>
            <Text> </Text>
                <Button onPress={() => this.props.navigation.navigate('ActivityIndicator')} title="Go to ActivityIndicator"></Button>
            <Text> </Text>
                <Button onPress={() => this.props.navigation.navigate('Button')} title="Go to Button"></Button>
            <Text> </Text>
                <Button onPress={() => this.props.navigation.navigate('DrawerLayout')} title="Go to Drawer Layer"></Button>
            <Text> </Text>
                <Button onPress={() => this.props.navigation.navigate('FlatList')} title="Go to FlatList"></Button>
            <Text> </Text>
                <Button onPress={() => this.props.navigation.navigate('Image')} title="Go to Image"></Button>
            <Text> </Text>
                <Button onPress={() => this.props.navigation.navigate('KeyboardAvoidingView')} title="Go to Keyboard Avoiding Component"></Button>
            <Text> </Text>
                <Button onPress={() => this.props.navigation.navigate('ListView')} title="Go to List view Component"></Button>
            <Text> </Text>
                <Button onPress={() => this.props.navigation.navigate('Modal')} title="Go to Modal Component"></Button>
            <Text> </Text>
                <Button onPress={() => this.props.navigation.navigate('Picker')} title="Go to Picker Component"></Button>
            <Text> </Text>
                <Button onPress={() => this.props.navigation.navigate('ProgressBar')} title="Go to Progress bar Component"></Button>
            <Text> </Text>
                <Button onPress={() => this.props.navigation.navigate('RefreshControl')} title="Go to Refresh control Component"></Button>
            <Text> </Text>
                <Button onPress={() => this.props.navigation.navigate('SafeArea')} title="Go to Safe area Component"></Button>
            <Text> </Text>
                <Button onPress={() => this.props.navigation.navigate('ScrollViewPage')} title="Go to Scroll View Component"></Button>
            <Text> </Text>
                <Button onPress={() => this.props.navigation.navigate('SectionList')} title="Go to Section List Component"></Button>
            <Text> </Text>
                <Button onPress={() => this.props.navigation.navigate('StatusBar')} title="Go to Status bar Component"></Button>
            <Text> </Text>
                <Button onPress={() => this.props.navigation.navigate('Switch')} title="Go to Switch Component"></Button>
            <Text> </Text>
                <Button onPress={() => this.props.navigation.navigate('Text_TextInp')} title="Go to Text and Text Input Component"></Button>
            <Text> </Text>
                <Button onPress={() => this.props.navigation.navigate('TouchableHighlight')} title="Go to TouchableHighlight"></Button>
            <Text> </Text>
                <Button onPress={() => this.props.navigation.navigate('ViewPage')} title="Go to View Component"></Button>
            <Text> </Text>
                <Button onPress={() => this.props.navigation.navigate('PageAndroid')} title="Go to View Page Android Component"></Button>
            <Text> </Text>
                <Button onPress={() => this.props.navigation.navigate('WebView')} title="Go to Web View Component"></Button>
            <Text> </Text>

            </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    butt: {
        borderRadius: 100
    }
})

export default LoginScreen;