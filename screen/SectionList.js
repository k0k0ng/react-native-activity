import React, { Component } from 'react';
import { SectionList, View, Text } from "react-native";

class SectionListPage extends Component {
    render() {
        return (
            <SectionList
            renderItem={({item, index, section}) => <Text key={index}>{item}</Text>}
            renderSectionHeader={({section: {title}}) => (
                <Text style={{fontWeight: 'bold'}}>{title}</Text>
            )}
            sections={[
                {title: 'Fruits', data: ['Devil Fruit', 'Banana']},
                {title: 'Places', data: ['Narnia', 'Dragon Valley']},
                {title: 'Foods', data: ['Monster meat', 'Dragon Egg']},
            ]}
            keyExtractor={(item, index) => item + index}
            />
        );
    }
}

export default SectionListPage;